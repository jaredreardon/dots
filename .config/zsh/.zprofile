# Add `~/.local/bin` to $PATH
export PATH="$PATH:${$(find ~/.local/bin -type d -printf %p:)%%:}"

# Default Programs
export EDITOR="nvim"
export TERMINAL="st"
export BROWSER="firefox"
export READER="zathura"
export FILE="vifm"
export IMAGE="sxiv"
export OPENER="xdg-open"
export PAGER="less"
export VIDEO="mpv"

# ~/ Clean-up:
export XDG_CONFIG_HOME="$HOME/.config"
export XDG_DATA_HOME="$HOME/.local/share"
export XDG_CACHE_HOME="$HOME/.cache"
export XINITRC="${XDG_CONFIG_HOME:-$HOME/.config}/x11/xinitrc"
export NOTMUCH_CONFIG="${XDG_CONFIG_HOME:-$HOME/.config}/notmuch-config"
export GTK2_RC_FILES="${XDG_CONFIG_HOME:-$HOME/.config}/gtk-2.0/gtkrc-2.0"
export LESSHISTFILE="-"
export WGETRC="${XDG_CONFIG_HOME:-$HOME/.config}/wget/wgetrc"
export INPUTRC="${XDG_CONFIG_HOME:-$HOME/.config}/shell/inputrc"
export ZDOTDIR="${XDG_CONFIG_HOME:-$HOME/.config}/zsh"
export ALSA_CONFIG_PATH="$XDG_CONFIG_HOME/alsa/asoundrc"
export GNUPGHOME="${XDG_DATA_HOME:-$HOME/.local/share}/gnupg"
export WINEPREFIX="${XDG_DATA_HOME:-$HOME/.local/share}/wineprefixes/default"
export KODI_DATA="${XDG_DATA_HOME:-$HOME/.local/share}/kodi"
export PASSWORD_STORE_DIR="${XDG_DATA_HOME:-$HOME/.local/share}/password-store"
export TMUX_TMPDIR="$XDG_RUNTIME_DIR"
export ANDROID_SDK_HOME="${XDG_CONFIG_HOME:-$HOME/.config}/android"
export CARGO_HOME="${XDG_DATA_HOME:-$HOME/.local/share}/cargo"
export GOPATH="${XDG_DATA_HOME:-$HOME/.local/share}/go"
export ANSIBLE_CONFIG="${XDG_CONFIG_HOME:-$HOME/.config}/ansible/ansible.cfg"
export UNISON="${XDG_DATA_HOME:-$HOME/.local/share}/unison"
export HISTFILE="${XDG_DATA_HOME:-$HOME/.local/share}/history"
export WEECHAT_HOME="${XDG_CONFIG_HOME:-$HOME/.config}/weechat"
export MBSYNCRC="${XDG_CONFIG_HOME:-$HOME/.config}/mbsync/config"
export ELECTRUMDIR="${XDG_DATA_HOME:-$HOME/.local/share}/electrum"

# This is the list for lf colors:
export LF_COLORS="\
*.png=33:\
*.webp=33:\
*.ico=33:\
*.jpg=33:\
*.jpe=33:\
*.jpeg=33:\
*.gif=33:\
*.svg=33:\
*.tif=33:\
*.tiff=33:\
*.mp3=35:\
*.opus=35:\
*.ogg=35:\
*.m4a=35:\
*.flac=35:\
*.wav=35:\
*.mkv=35:\
*.mp4=35:\
*.webm=35:\
*.mpeg=35:\
*.avi=35:\
*.mov=35:\
*.mpg=35:\
*.wmv=35:\
*.m4b=35:\
*.flv=35:\
*.csv=31:\
*.xlsx=31:\
*.tex=31:\
*.md=31:\
*.r=31:\
*.R=31:\
*.rmd=31:\
*.Rmd=31:\
*.m=31:\
*.sent=31:\
"

# This is the list for lf icons:
export LF_ICONS="di=📁:\
fi=📃:\
tw=🤝:\
ow=📂:\
ln=⛓ :\
or=❌:\
ex=🎯:\
*.txt=✍ :\
*.mom=✍ :\
*.me=✍ :\
*.ms=✍ :\
*.png=🖼 :\
*.webp=🖼 :\
*.ico=🖼 :\
*.jpg=📸:\
*.jpe=📸:\
*.jpeg=📸:\
*.gif=🖼 :\
*.svg=🗺:\
*.tif=🖼 :\
*.tiff=🖼 :\
*.xcf=🖌:\
*.html=🌎:\
*.xml=📰:\
*.gpg=🔒:\
*.css=🎨:\
*.pdf=📚:\
*.djvu=📚:\
*.epub=📚:\
*.csv=📓:\
*.xlsx=📓:\
*.tex=📜:\
*.md=📘:\
*.r=📊:\
*.R=📊:\
*.rmd=📊:\
*.Rmd=📊:\
*.m=📊:\
*.mp3=🎵:\
*.opus=🎵:\
*.ogg=🎵:\
*.m4a=🎵:\
*.flac=🎼:\
*.wav=🎼:\
*.mkv=🎥:\
*.mp4=🎥:\
*.webm=🎥:\
*.mpeg=🎥:\
*.avi=🎥:\
*.mov=🎥:\
*.mpg=🎥:\
*.wmv=🎥:\
*.m4b=🎥:\
*.flv=🎥:\
*.zip=📦:\
*.rar=📦:\
*.7z=📦:\
*.tar.gz=📦:\
*.z64=🎮:\
*.v64=🎮:\
*.n64=🎮:\
*.gba=🎮:\
*.nes=🎮:\
*.gdi=🎮:\
*.1=ℹ :\
*.nfo=ℹ :\
*.info=ℹ :\
*.log=📙:\
*.iso=📀:\
*.img=📀:\
*.bib=🎓:\
*.ged=👪:\
*.part=💔:\
*.torrent=🔽:\
*.jar=♨:\
*.java=♨:\
"

# Other program settings:
export DICS="/usr/share/stardict/dic/"

_gen_fzf_default_opts() {

local color00='#000000'
local color01='#1E1E1E'
local color02='#282828'
local color03='#767676'
local color04='#808080'
local color05='#BBBBBB'
local color06='#AAAAAA'
local color07='#FFFFFF'
local color08='#CC0403'
local color09='#CC6903'
local color0A='#CECB00'
local color0B='#19CB00'
local color0C='#14FFFF'
local color0D='#0D73CC'
local color0E='#FD28FF'
local color0F='#6E4B3D'

export FZF_DEFAULT_OPTS="$FZF_DEFAULT_OPTS"\
"--layout=reverse"\
" --color=bg+:$color01,bg:$color00,spinner:$color0C,hl:$color0D"\
" --color=fg:$color04,header:$color0D,info:$color0A,pointer:$color0C"\
" --color=marker:$color0C,fg+:$color06,prompt:$color0A,hl+:$color0D"

}

_gen_fzf_default_opts

export LESS=-R
export LESS_TERMCAP_mb=$'\e[1;32m'
export LESS_TERMCAP_md=$'\e[1;32m'
export LESS_TERMCAP_me=$'\e[0m'
export LESS_TERMCAP_se=$'\e[0m'
export LESS_TERMCAP_so=$'\e[01;33m'
export LESS_TERMCAP_ue=$'\e[0m'
export LESS_TERMCAP_us=$'\e[1;4;31m'
export LESSOPEN="| /usr/bin/highlight -O ansi %s 2>/dev/null"
export MOZ_USE_XINPUT2="1"		# Mozilla smooth scrolling/touchpads.
export AWT_TOOLKIT="MToolkit wmname LG3D"	#May have to install wmname
export _JAVA_AWT_WM_NONREPARENTING=1	# Fix for Java applications in dwm
export LC_COLLATE="C"
export NNN_FIFO=/tmp/nnn.fifo
export NNN_FCOLORS='030304020006060801050501'
export NNN_PLUG='d:dragdrop;x:xdgdefault;w:setbg;p:previewer'
export NNN_BMS='h:~;p:~/pix;w:~/pix/wal;d:~/dox;u:~/dox/uni;y:~/vid/yt;a:~/vid/tv;m:~/vid/mov;c:~/.config;s:~/.local/bin'

# Shortcuts
[ ! -f ${XDG_CONFIG_HOME:-$HOME/.config}/shell/shortcutrc ] && shortcuts >/dev/null 2>&1 &

# Autostart
[ "$(tty)" = "/dev/tty1" ] && ! pidof -s Xorg >/dev/null 2>&1 && exec startx "$XDG_CONFIG_HOME/x11/xinitrc"

#if [ -z $DISPLAY ] && [ "$(tty)" = "/dev/tty1" ]; then
#  exec sway
#fi
