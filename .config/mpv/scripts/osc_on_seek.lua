-- emulate require() path search but load into current scope instead.
-- osc.lua does not export a module table, so require would just return True.
for key, loader in pairs(package.loaders) do
    res = loader('@osc.lua')
    print('res value', '"', res, '"', type(res))
    if type(res) == 'function' then
        print('found', res, res('@osc.lua'))
        break
    end
end

function seek_handler()
    show_osc()
end
mp.register_event("seek", seek_handler)
