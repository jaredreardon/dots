let mapleader =","

" Vim-Plug
if ! filereadable(system('echo -n "${XDG_CONFIG_HOME:-$HOME/.config}/nvim/autoload/plug.vim"'))
	echo "Downloading junegunn/vim-plug to manage plugins..."
	silent !mkdir -p ${XDG_CONFIG_HOME:-$HOME/.config}/nvim/autoload/
	silent !curl "https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim" > ${XDG_CONFIG_HOME:-$HOME/.config}/nvim/autoload/plug.vim
	autocmd VimEnter * PlugInstall
endif

" Plugins
call plug#begin(system('echo -n "${XDG_CONFIG_HOME:-$HOME/.config}/nvim/plugged"'))
    Plug 'jaredreardon/kitty.vim'
    Plug 'junegunn/goyo.vim'
    Plug 'junegunn/limelight.vim'
	Plug 'vifm/vifm.vim'
    Plug 'ap/vim-css-color'
	Plug 'junegunn/vim-emoji'
call plug#end()

" Colorscheme
    set termguicolors
    set background=dark
    colorscheme kitty

" Basic Settings
	nnoremap c "_c
	set nocompatible
	filetype plugin on
	syntax on
	set encoding=utf-8
    set number relativenumber
	set title
    set linebreak
	set mouse=a
	set nohlsearch
	set clipboard+=unnamedplus
	set laststatus=0
	set tabstop=4	softtabstop=4
    set shiftwidth=4
	set expandtab
    set smartindent
    set cursorline

" Text Wrapping
    au BufRead,BufNewFile *.md,*.rmd,*.tex,*.txt,*.ms setlocal textwidth=95

" Goyo
    map <leader><ENTER> :Goyo<CR>

    function! s:goyo_enter()
        set noshowmode
        set noshowcmd
        set scrolloff=999
        Limelight
    endfunction

    function! s:goyo_leave()
        set showcmd
        set scrolloff=5
        Limelight!
    endfunction

" Limelight
    autocmd! User GoyoEnter Limelight
    autocmd! User GoyoLeave Limelight!

    let g:limelight_conceal_ctermfg = 240

" Remove unnecessary g keypresses
    noremap <silent> 0 g0
    noremap <silent> $ g$

" Disable arrow and page keys
    noremap     <Up>        <nop>
    noremap     <Down>      <nop>
    noremap     <Left>      <nop>
    noremap     <Right>     <nop>
    noremap     <PageUp>    <nop>
    noremap     <PageDown>  <nop>
    inoremap    <PageUp>    <nop>
    inoremap    <PageDown>  <nop>

" Autocompletion
	set wildmode=longest,list,full

" Disables automatic commenting on newline
	autocmd FileType * setlocal formatoptions-=c formatoptions-=r formatoptions-=o

" Spell-check set to <leader>o, 'o' for 'orthography'
	map <leader>o :setlocal spell! spelllang=en_us<CR>

" Open splits at the bottom and right
	set splitbelow splitright

" Shortcut split navigation
	map <C-h> <C-w>h
	map <C-j> <C-w>j
	map <C-k> <C-w>k
	map <C-l> <C-w>l

" Replace ex mode with gq
	map Q gq

" Check file in shellcheck
	map <leader>s :!clear && shellcheck %<CR>

" Open my bibliography file in split
	map <leader>b :vsp<space>$BIB<CR>
	map <leader>r :vsp<space>$REFER<CR>

" Replace all is aliased to S
	nnoremap S :%s//g<Left><Left>

" Compile document, be it groff/LaTeX/markdown/etc...
	map <leader>c :w! \|:silent !compiler <c-r>%<CR>

" Open pdf or html or preview
	map <leader>p :silent !opout <c-r>%<CR><CR>

" Clean tex build files when exiting a .tex file
	autocmd VimLeave *.tex !texclear %

" LaTex template files
	map <leader>rp :0r $HOME/dox/latex/rp.tex<CR>
	map <leader>dc :0r $HOME/dox/latex/doc.tex<CR>
	map <leader>pr :0r $HOME/dox/latex/pres.tex<CR>

" Ensure files are read as what I want
	autocmd BufRead,BufNewFile /tmp/calcurse*,~/.calcurse/notes/* set filetype=markdown
	autocmd BufRead,BufNewFile *.ms,*.me,*.mom,*.man set filetype=groff
	autocmd BufRead,BufNewFile *.tex set filetype=tex

" Save file as sudo on files that require root permission
	cnoremap w!! execute 'silent! write !sudo tee % >/dev/null' <bar> edit!

" Goyo for (neo)mutt email composition
	autocmd BufRead,BufNewFile /tmp/neomutt* let g:goyo_width=80
	autocmd BufRead,BufNewFile /tmp/neomutt* :Goyo | set bg=dark
	autocmd BufRead,BufNewFile /tmp/neomutt* map ZZ :Goyo\|x!<CR>
	autocmd BufRead,BufNewFile /tmp/neomutt* map ZQ :Goyo\|q!<CR>

" Automatically deletes all trailing whitespace and newlines at end of file on save
	autocmd BufWritePre * %s/\s\+$//e
	autocmd BufWritepre * %s/\n\+\%$//e

" When shortcut files are updated, renew bash and ranger configs with new material
	autocmd BufWritePost files,directories !shortcuts
" Run xrdb whenever Xdefaults or Xresources are updated
	autocmd BufWritePost *Xresources,*Xdefaults !xrdb %

" Turns off highlighting on the bits of code that are changed, so the line that is changed is highlighted but the actual text that has changed stands out on the line and is readable
    if &diff
        highlight! link DiffText MatchParen
    endif

" Transparent line numbers for unselected lines
    highlight LineNr guibg=NONE
