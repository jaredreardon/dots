# Kitty Color Scheme for vim/nvim

This color scheme is a direct fork of Mike Hartington's awesome
[OceanicNext](https://github.com/mhartington/oceanic-next) color scheme.
Literally all I did was change the colors and the branding to match the
Kitty colors. All credit goes to Mike Hartington for creating this color
scheme originally (and making it super easy to customize!) So thanks to
him!

The Kitty color scheme is a vim theme inspired by the colors from the
[Kitty terminal](https://github.com/kovidgoyal/kitty). I have only changed
minor things from the default Kitty terminal colors (such as changing the
foreground text color to be a bit dimmer and adding more colors like orange
for the sake of completion). Otherwise, this is a faithful port of the
    Kitty terminal colors to vim for those who enjoy this color scheme.
