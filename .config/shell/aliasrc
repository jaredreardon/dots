#!/bin/sh

# Use neovim for vim if present.
[ -x "$(command -v nvim)" ] && alias vim="nvim" vimdiff="nvim -d"

# Make verbose command flags more concise
alias \
	cp="cp -iv" \
	mv="mv -iv" \
	rm="rm -vI" \
    df="df -h" \
	bc="bc -ql" \
	mkdir="mkdir -pv" \
	7zu="7z x" \
	yt="yt-dlp -f 'bestvideo[ext=mp4]+bestaudio[ext=m4a]/mp4' --add-metadata -i -o '%(title)s.%(ext)s'" \
	yta="yt-dlp -o '%(title)s.%(ext)s' -f 251 -x --extract-audio --audio-format 'opus'" \
    km="kcc-c2e --profile=K578 --format=EPUB -m --upscale --stretch" \
	ffmpeg="ffmpeg -hide_banner"

# Colorization
alias \
	ls="LC_COLLATE=C ls --color=always --group-directories-first" \
	grep="grep --color=auto" \
	diff="diff --color=auto" \
    ip="ip -color=auto"

# Abbreviate long commands
alias \
    ..="cd .." \
	ka="killall" \
    mpv="mpv --no-osd-bar" \
    lf="lfub" \
    nnn="nnn -Rd" \
	sdn="sudo shutdown -h now" \
    nukepkg="pacman -Qtdq | sudo pacman -Rns -" \
    speed="speedtest-cli --secure" \
    wtr="clear && curl 'wttr.in/?F'" \
    rate="curl 'rate.sx/?F'" \
    define="tuxi -q define" \
    dots="/usr/bin/git --git-dir=$HOME/.local/src/dots/ --work-tree=$HOME" \
    sudo="sudo " \
    rootcron="sudo env EDITOR=nvim crontab -e -u root" \
    p="paru --skipreview" \
	xi="xbps-install" \
	xr="xbps-remove -R" \
	xq="xbps-query"

# fzf Commands
se() { du -a $HOME/.local/bin/* | awk '{print $2}' | fzf | xargs -r $EDITOR ;}
ce() { du -a $XDG_CONFIG_HOME/* | awk '{print $2}' | fzf | xargs -r $EDITOR ;}
pe() { du -a $HOME/dox/uni/* | awk '{print $2}' | fzf | xargs -r $EDITOR ;}
