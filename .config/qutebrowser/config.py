# Disable autoconfig
config.load_autoconfig(False)

# Adblock
c.content.blocking.enabled = True
c.content.blocking.method = 'adblock'
c.content.blocking.hosts.lists = ['https://raw.githubusercontent.com/StevenBlack/hosts/master/hosts']
c.content.blocking.adblock.lists = [
        "https://easylist.to/easylist/easylist.txt",
        "https://easylist.to/easylist/easyprivacy.txt",
        "https://easylist.to/easylist/fanboy-social.txt",
        "https://secure.fanboy.co.nz/fanboy-annoyance.txt",
        "https://easylist-downloads.adblockplus.org/abp-filters-anti-cv.txt",
        #"https://gitlab.com/curben/urlhaus-filter/-/raw/master/urlhaus-filter.txt",
        "https://pgl.yoyo.org/adservers/serverlist.php?showintro=0;hostformat=hosts",
        "https://github.com/uBlockOrigin/uAssets/raw/master/filters/legacy.txt",
        "https://github.com/uBlockOrigin/uAssets/raw/master/filters/filters.txt",
        "https://github.com/uBlockOrigin/uAssets/raw/master/filters/filters-2020.txt",
        "https://github.com/uBlockOrigin/uAssets/raw/master/filters/filters-2021.txt",
        "https://github.com/uBlockOrigin/uAssets/raw/master/filters/badware.txt",
        "https://github.com/uBlockOrigin/uAssets/raw/master/filters/privacy.txt",
        "https://github.com/uBlockOrigin/uAssets/raw/master/filters/badlists.txt",
        "https://github.com/uBlockOrigin/uAssets/raw/master/filters/annoyances.txt",
        "https://github.com/uBlockOrigin/uAssets/raw/master/filters/resource-abuse.txt",
        "https://www.i-dont-care-about-cookies.eu/abp/",
        "https://secure.fanboy.co.nz/fanboy-cookiemonster.txt",
        "https://github.com/uBlockOrigin/uAssets/raw/master/filters/unbreak.txt"]

# UI
c.colors.webpage.preferred_color_scheme = "dark"
c.completion.timestamp_format = "%m-%d-%Y %H:%M"

# Don't show bookmarks, history, etc on open command
c.completion.open_categories = ["filesystem"]

# Remove Unnecessary Animations
c.content.prefers_reduced_motion = True

# Automatically Restore Tabs
c.auto_save.session = True

# Downloads
c.downloads.location.directory = '$HOME/dl'
c.downloads.position = 'bottom'
c.downloads.remove_finished = 1

# File Selector
c.fileselect.handler = 'external'
c.fileselect.single_file.command = ["st", "-n", "spvifm", "-g", "100x30", "-e", "vifm", "--choose-files={}"]
c.fileselect.multiple_files.command = ["st", "-n", "spvifm", "-g", "100x30", "-e", "vifm", "--choose-files={}"]

# Editor
c.editor.command = ["st", "-e", "nvim", "{file}", "-c", "normal {line}G{column0}l"]

# Tab Bar and Statusbar (always, never, multiple, or switching)
c.tabs.show = 'always'
c.tabs.title.format_pinned = "{audio}"
c.tabs.title.format = "{audio}{current_title}"
c.tabs.indicator.width = 1
c.tabs.favicons.scale = 0.75
c.statusbar.padding = {'top': 3, 'bottom': 3, 'left': 1, 'right': 3}
c.statusbar.show = 'always'
c.tabs.padding = {'top': 3, 'bottom': 3, 'left': 3, 'right': 0}

# Startup and Default Page
c.url.default_page = 'file:///home/jared/.config/qutebrowser/startpage/start.html'
c.url.start_pages = 'file:///home/jared/.config/qutebrowser/startpage/start.html'

# Font Settings
c.fonts.default_family = 'Sans'
c.fonts.default_size = '12pt'
c.fonts.web.size.default = 16
c.fonts.web.size.default_fixed = 12
c.fonts.web.size.minimum = 0
c.fonts.web.size.minimum_logical = 6

# Default Web Background Color
c.colors.webpage.bg = 'Black'

# Smooth Scrolling
c.scrolling.smooth = False

# Disable Scrollbar
config.set('scrolling.bar', 'never')

# Internet Privacy and Usability Improvements
c.content.default_encoding = 'utf-8'
c.content.cookies.accept = 'no-3rdparty'
c.content.cookies.store = True
c.content.dns_prefetch = True

# Spellcheck
#c.spellcheck.languages = ["en-US"]

# Password Management
#config.bind('ee','spawn --userscript qute-pass')
#config.bind('eu','spawn --userscript qute-pass --username-only')
#config.bind('ep','spawn --userscript qute-pass --password-only')
#config.bind('eo','spawn --userscript qute-pass --otp-only')

# Search Engines
c.url.searchengines = {'DEFAULT': 'https://searx.xyz/?q={}',
                        'ddg': 'https://duckduckgo.com/?q={}',
                        'go': 'https://www.google.com/search?q={}',
                        'yt': 'https://www.yewtu.be/search?q={}',
                        'aw': 'https://wiki.archlinux.org/?search={}',
                        'd': 'https://www.merriam-webster.com/dictionary/{}',
                        'w': 'https://en.wikipedia.org/wiki/{}'}

# Fix Searx Hinting
with config.pattern('searx.xyz') as p:
    p.hints.selectors['all'].append('label[for^="checkbox_"]')

# Normal Mode Bindings
config.bind('M', 'hint links spawn --detach mpv --force-window yes {hint-url}')
config.bind('Z', 'hint links spawn st -e yt-dlp {hint-url}')
config.bind('xb', 'config-cycle statusbar.show always never')
config.bind('xt', 'config-cycle tabs.show always never')
config.bind('xx', 'config-cycle statusbar.show always never;; config-cycle tabs.show always never')
config.bind('t', 'open -t')
config.bind('pt', 'tab-pin')

# Improve J/K Keys
config.bind('J', 'tab-prev')
config.bind('K', 'tab-next')

config.bind('j', 'scroll-px 0 100')
config.bind('k', 'scroll-px 0 -100')

config.bind('gJ', 'tab-move -')
config.bind('gK', 'tab-move +')

## Xresources Colors
import subprocess
def read_xresources(prefix):
    props = {}
    x = subprocess.run(['xrdb', '-query'], stdout=subprocess.PIPE)
    lines = x.stdout.decode().split('\n')
    for line in filter(lambda l : l.startswith(prefix), lines):
        prop, _, value = line.partition(':\t')
        props[prop] = value
    return props
xresources = read_xresources('*')

c.colors.statusbar.normal.bg = xresources['*.background']
c.colors.completion.category.bg = xresources['*.background']
c.colors.completion.category.border.bottom = xresources['*.background']
c.colors.completion.category.border.top = xresources['*.background']
c.colors.completion.category.fg = xresources['*.color1']
c.colors.completion.even.bg = xresources['*.background']
c.colors.completion.odd.bg = xresources['*.background']
c.colors.completion.fg = xresources['*.foreground']
c.colors.completion.item.selected.bg = xresources['*.color4']
c.colors.completion.item.selected.border.bottom = xresources['*.color4']
c.colors.completion.item.selected.border.top = xresources['*.color4']
c.colors.completion.item.selected.fg = xresources['*.color15']
c.colors.completion.item.selected.match.fg = xresources['*.color15']
c.colors.completion.match.fg = xresources['*.color3']
c.colors.completion.scrollbar.bg = xresources['*.background']
c.colors.completion.scrollbar.fg = xresources['*.background']
c.colors.downloads.bar.bg = xresources['*.background']
c.colors.downloads.error.bg = xresources['*.background']
c.colors.downloads.error.fg = xresources['*.color1']
c.colors.downloads.stop.bg = xresources['*.background']
c.colors.downloads.system.bg = 'none'
c.colors.hints.bg = xresources['*.background']
c.colors.hints.fg = xresources['*.color7']
c.hints.border = '2px solid ' + xresources['*.color3']
c.colors.hints.match.fg = xresources['*.color4']
c.colors.keyhint.bg = xresources['*.background']
c.colors.keyhint.fg = xresources['*.color4']
c.colors.keyhint.suffix.fg = xresources['*.color15']
c.colors.messages.error.bg = xresources['*.background']
c.colors.messages.error.border = xresources['*.background']
c.colors.messages.error.fg = xresources['*.color1']
c.colors.messages.info.bg = xresources['*.background']
c.colors.messages.info.border = xresources['*.background']
c.colors.messages.info.fg = xresources['*.color4']
c.colors.messages.warning.bg = xresources['*.background']
c.colors.messages.warning.border = xresources['*.background']
c.colors.messages.warning.fg = xresources['*.color1']
c.colors.prompts.bg = xresources['*.background']
c.colors.prompts.border = '0px solid ' + xresources['*.background']
c.colors.prompts.fg = xresources['*.color4']
c.colors.prompts.selected.bg = xresources['*.color15']
c.colors.statusbar.caret.bg = xresources['*.background']
c.colors.statusbar.caret.fg = xresources['*.color1']
c.colors.statusbar.caret.selection.bg = xresources['*.background']
c.colors.statusbar.caret.selection.fg = xresources['*.color1']
c.colors.statusbar.command.bg = xresources['*.background']
c.colors.statusbar.command.fg = xresources['*.color5']
c.colors.statusbar.command.private.bg = xresources['*.background']
c.colors.statusbar.command.private.fg = xresources['*.foreground']
c.colors.statusbar.insert.bg = xresources['*.color0']
c.colors.statusbar.insert.fg = xresources['*.foreground']
c.colors.statusbar.normal.bg = xresources['*.background']
c.colors.statusbar.normal.fg = xresources['*.foreground']
c.colors.statusbar.passthrough.bg = xresources['*.background']
c.colors.statusbar.passthrough.fg = xresources['*.color1']
c.colors.statusbar.private.bg = xresources['*.background']
c.colors.statusbar.private.fg = xresources['*.foreground']
c.colors.statusbar.progress.bg = xresources['*.foreground']
c.colors.statusbar.url.error.fg = xresources['*.color1']
c.colors.statusbar.url.fg = xresources['*.color2']
c.colors.statusbar.url.hover.fg = xresources['*.color6']
c.colors.statusbar.url.success.http.fg = xresources['*.color2']
c.colors.statusbar.url.success.https.fg = xresources['*.color2']
c.colors.statusbar.url.warn.fg = xresources['*.color3']
c.colors.tabs.bar.bg = xresources['*.background']
c.colors.tabs.even.bg = xresources['*.colorExtra']
c.colors.tabs.even.fg = xresources['*.color7']
c.colors.tabs.indicator.error = xresources['*.color1']
c.colors.tabs.indicator.start = xresources['*.color1']
c.colors.tabs.indicator.stop = xresources['*.color2']
c.colors.tabs.indicator.system = 'none'
c.colors.tabs.odd.bg = xresources['*.colorExtra']
c.colors.tabs.odd.fg = xresources['*.color7']
c.colors.tabs.selected.even.bg = xresources['*.background']
c.colors.tabs.selected.even.fg = xresources['*.color15']
c.colors.tabs.selected.odd.bg = xresources['*.background']
c.colors.tabs.selected.odd.fg = xresources['*.color15']
c.colors.tabs.pinned.even.bg = xresources['*.colorExtra']
c.colors.tabs.pinned.even.fg = xresources['*.foreground']
c.colors.tabs.pinned.odd.bg = xresources['*.colorExtra']
c.colors.tabs.pinned.odd.fg = xresources['*.foreground']
c.colors.tabs.pinned.selected.even.bg = xresources['*.background']
c.colors.tabs.pinned.selected.even.fg = xresources['*.color15']
c.colors.tabs.pinned.selected.odd.bg = xresources['*.background']
c.colors.tabs.pinned.selected.odd.fg = xresources['*.color15']
